# CITY GAME

This is a City Game that I build for **United Cuisines**

For this purpose I have used *Next.js* as Javascript framework and the *Google Maps API* for working with the map and coordinates.

You can visit the app here, [https://juego-ciudades-paises.vercel.app](https://juego-ciudades-paises.vercel.app)

If you want to see more, visit my amazing portfolio at [https://dcano.dev](https://www.dcano.dev)