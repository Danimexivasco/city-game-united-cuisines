import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';
import getConfig from "next/config";
import Swal from 'sweetalert2';

import { css, Global } from '@emotion/core';
import MapStyles from '../components/MapStyles';
import cities from '../cities.json';
import { Container, Footer, Info, Map, StyledMessage, StyledMessageWrong, StyledButton, PlayAgainButton } from '../components/StyledComponents';


const mapContainerStyle = {
  width: '100%',
  height: '50vh'
}

const center = {
  lat: 49.526,
  lng: 14.2551
}

const options = {
  styles: MapStyles,
  disableDefaultUI: true,
  zoomControl: true,
  maxZoom: 6,
  minZoom: 4,
}

const capitalCitiesJSON = cities.capitalCities;

const haversine_distance = (questionObjective, userAnswer) => {
  // console.info(questionObjective)
  // console.info(userAnswer)
  var R = 6371.0710; // Radius of the Earth in kms
  var rlat1 = questionObjective.lat * (Math.PI / 180); // Convert degrees to radians
  var rlat2 = userAnswer.lat * (Math.PI / 180); // Convert degrees to radians
  var difflat = rlat2 - rlat1; // Radian difference (latitudes)
  var difflon = (userAnswer.lng - questionObjective.long) * (Math.PI / 180); // Radian difference (longitudes)

  var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));
  return d;
}

export default function Home() {

  // Env variables config
  const { publicRuntimeConfig } = getConfig();
  const { googleAPI } = publicRuntimeConfig;

  // STATE
  const [kmLeft, setKmLeft] = useState(1500);
  const [questionNumber, setQuestionNumber] = useState(0);
  const [objective, setObjective] = useState(capitalCitiesJSON[0]);
  const [objectiveMarker, setObjectiveMarker] = useState(null);
  const [answer, setAnswer] = useState(null);
  const [correctly, setCorrectly] = useState(false);
  const [correct, setCorrect] = useState(0);
  const [incorrect, setIncorrect] = useState(0);
  const [msg, setMsg] = useState("");
  const [gameOver, setGameOver] = useState(false);
  const [end, setEnd] = useState(false);


  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: googleAPI
  })

  // When questionNumber changes, the objective changes
  useEffect(() => {
    if (questionNumber < capitalCitiesJSON.length) {
      setObjective(capitalCitiesJSON[questionNumber])
    }
  }, [questionNumber])

  // Check the kmLef to see if the player lose
  useEffect(() => {
    if (kmLeft <= 0) {
      setGameOver(true)
      Swal.fire({
        title: 'YOU LOSE',
        text: `You have placed ${correct} cities well. Do you want to retry it?`,
        showConfirmButton: true,
        confirmButtonText: 'Retry',
        imageUrl: '/skull.svg',
        imageWidth: 200,
        imageHeight: 300,
        imageAlt: 'Mexican Skull',
      }).then((result) => {
        if (result.value) {
          setKmLeft(1500)
          setQuestionNumber(0)
          setObjective(null)
          setObjectiveMarker(null)
          setAnswer(null)
          setMsg("")
          setCorrect(0)
          setIncorrect(0)
          setGameOver(false)
          setEnd(false)
        }
      })
    }
  }, [kmLeft, gameOver])

  // Verify the map loading
  if (loadError) return "Error loading maps";
  if (!isLoaded) return "Loading Map...";

  // Fn that verifies the user answers
  const verifyAnswer = (objective, answer) => {
    const distance = haversine_distance(objective, answer);

    if (distance < 50) {
      setCorrectly(true);
      setCorrect(correct + 1)
      setMsg(`Wow! You're really good on that! You stayed only to ${distance.toFixed(2)} km`)
      setTimeout(() => {
        setMsg("");
        setCorrectly(false)
      }, 3000);
    } else {
      setKmLeft(kmLeft - distance < 0 ? 0 : kmLeft - distance)
      setIncorrect(incorrect + 1)
      setMsg(`You haven't even been close... You have failed by ${distance.toFixed(2)} km! `)
      setTimeout(() => {
        setMsg("");
        setCorrectly(false)
      }, 3000);
    }
    setObjectiveMarker({
      lat: Number(objective.lat),
      lng: Number(objective.long)
    })

    setTimeout(() => {
      setAnswer(null);
      setObjectiveMarker(null);
      if ((questionNumber < capitalCitiesJSON.length - 1) && (kmLeft > 0)) {
        setQuestionNumber(questionNumber + 1);
      } else {
        //When the game over, two options
        // You run out of kms, you lose
        if (kmLeft <= 0) {
          console.log('%c You lose!', 'font-size: 20px; color: red;');
        }
        // You finish the game, so you win
        else {
          console.log('%c You win!', 'font-size: 20px; color: green;');
          setEnd(true);
          Swal.fire({
            title: 'Congratulations, you did it!!',
            text: `You are better placed than Christopher Columbus. Distance remain: ${kmLeft.toFixed(2)} km. Well placed cities: ${correct}`,
            showConfirmButton: true,
            confirmButtonText: 'Play again',
            imageUrl: '/cup.svg',
            imageWidth: 300,
            imageHeight: 300,
            imageAlt: 'Winner Cup',
          }).then((result) => {
            if (result.value) {
              setKmLeft(1500)
              setQuestionNumber(0)
              setObjective(null)
              setObjectiveMarker(null)
              setAnswer(null)
              setMsg("")
              setCorrect(0)
              setIncorrect(0)
              setGameOver(false)
              setEnd(false)
            }
          })
        }
      }
    }, 3000);
  }

  // The replay fn restart all the State to their default values
  const replay = () => {
    setKmLeft(1500)
    setQuestionNumber(0)
    setObjective(null)
    setObjectiveMarker(null)
    setAnswer(null)
    setMsg("")
    setCorrect(0)
    setIncorrect(0)
    setGameOver(false)
    setEnd(false)
  }

  return (
    <>
      <Global
        styles={css`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
            Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
          box-sizing: border-box;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }

        a {
          color: inherit;
          text-decoration: none;
        }
      `}
      />
      <Container>
        <Head>
          <title>City Game</title>
          <link rel="icon" href="/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale =1.0, maximum-scale=1.0, user-scalable=no" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" />
        </Head>

        <Info>
          <h1>City Game</h1>
          <div>
            <b>{questionNumber}</b> cities placed
          </div>
          <div>
            <b>{kmLeft.toFixed(0)}</b> kilometers left
          </div>
          {(!end && !gameOver && objective !== null) && (
            <p>Select the location of <b>"{objective.capitalCity}"</b></p>
          )}
        </Info>
        {(!end && !gameOver) ? (
          <>
            <Map>
              <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={4}
                center={center}
                options={options}
                onClick={(e) => {
                  setAnswer({
                    lat: e.latLng.lat(),
                    lng: e.latLng.lng()
                  })
                }}
              >
                {(answer) && (
                  <Marker
                    key={answer.lat}
                    position={{ lat: answer.lat, lng: answer.lng }}
                    icon={{
                      url: "/target.svg",
                      scaledSize: new window.google.maps.Size(30, 30),
                      origin: new window.google.maps.Point(0, 0),
                      anchor: new window.google.maps.Point(15, 15)
                    }}
                  />)}
                {(objectiveMarker) && (
                  <Marker
                    key={objectiveMarker.lat}
                    position={{ lat: objectiveMarker.lat, lng: objectiveMarker.lng }}
                  />)}

              </GoogleMap>
            </Map>
            {msg && (
              correctly ? (
                <StyledMessage>{msg}</StyledMessage>
              ) : (
                  <StyledMessageWrong>{msg}</StyledMessageWrong>
                )
            )}
            <StyledButton
              onClick={() => verifyAnswer(objective, answer)}
            >Place</StyledButton>
          </>
        ) : (
            end ? (
              <PlayAgainButton
                onClick={() => replay()}
              >Play Again</PlayAgainButton>
            ) : (
                <PlayAgainButton
                  onClick={() => replay()}
                >Retry</PlayAgainButton>
              )
          )}

        <Footer>
          <p>Build by <b>Daniel Cano</b> for <i>United Cuisines</i></p>
        </Footer>
      </Container>
    </>
  )
}