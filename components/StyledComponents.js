import styled from '@emotion/styled';

export const Container = styled.div`
  min-height: 100vh;
  padding: 0 0.5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Footer = styled.footer`
  width: 100%;
  height: 100px;
  border-top: 1px solid #eaeaea;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  /* position:absolute;
  bottom:0; */
`;

export const Info = styled.div`
  /* position: absolute;
  top: 1rem; */
  text-align: center;
  width: 90%;
  max-width: 695px;
  h1{
    text-transform: uppercase;
    /* margin-bottom: 2rem; */
  }
  div{
    margin: 0 auto 1.5rem auto;
    width: 100%;
    border: 1px solid #e1e1e1;
    padding: 1rem 2.5rem;
  }
  p{
    margin-bottom: 2rem;
  }
`;

export const Map = styled.div`
  margin: 0 auto;
  width: 90%;
  border: 1px solid #e1e1e1;
  svg{
    height: 100%;
  }
`;

export const StyledMessage = styled.p`
  text-align: center;
  padding: 1rem 2rem;
  width: 90%;
  background-color: #7DB588;
  color: #FFFFFF;
  @media (max-width: 770px){
      width: 90%
  }
`;

export const StyledMessageWrong = styled.p`
  text-align: center;
  padding: 1rem 2rem;
  width: 90%;
  background-color: #C31D28;
  color: #FFFFFF;
  @media (max-width: 770px){
      width: 90%
  }
`;

export const StyledButton = styled.button`
  padding: 0;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  margin-bottom: 2rem;
  margin-top: 2rem; 
  border: solid 1px transparent;
  border-radius: 4px;
  padding: 1rem 2rem;
  color: #EFFFFA;
  background-color: #1E1F1F;
  text-transform: uppercase;
  letter-spacing: 0.11rem;
  font-size: 1.4rem;
  transition: all 0.2s linear;
  width: 80%;
  
  &:hover{
	background-color: #646868;
  
  }
  &:focus{
	outline: none;
  }

  @media (min-width: 770px){
    width: 25%;
    /* align-self: flex-end;
    margin-right: 5%; */
  }
`;

export const PlayAgainButton = styled.button`
  padding: 0;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  margin-bottom: 20rem;
  margin-top: 2rem; 
  border: solid 1px transparent;
  border-radius: 4px;
  padding: 1rem 2rem;
  color: #EFFFFA;
  background-color: #1E1F1F;
  text-transform: uppercase;
  letter-spacing: 0.11rem;
  font-size: 1.4rem;
  transition: all 0.2s linear;
  width: 80%;
  
  &:hover{
	background-color: #646868;
  
  }
  &:focus{
	outline: none;
  }

  @media (min-width: 770px){
    width: 30%;
  }
`;